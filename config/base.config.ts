import path from 'path';
import webpack, {Configuration} from 'webpack';
import { VueLoaderPlugin } from 'vue-loader';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';

/**
 * apply environment
 */
const APP_TARGET = process.env.APP_TARGET || 'client';
const PROD = true; // process.env.NODE_ENV === 'production';
const BUILD_PATH = path.resolve(__dirname, `../dist/${APP_TARGET}/`);
const PUBLIC_PATH = '/resort/';
const MINIFY = true;

export default <Configuration>{
    mode: PROD ? 'production' : 'development',
    devtool: PROD ? false : 'source-map',
    // stats: {
    //     entrypoints: false,
    //     cachedAssets: false,
    //     children: false,
    //     assets: false,
    //     publicPath: false,
    //     modules: false,
    //     moduleTrace: false,
    //     outputPath: true,
    //     version: true,
    // },
    output: {
        path: BUILD_PATH,
        publicPath: PUBLIC_PATH,
        assetModuleFilename: 'asset/[name].[hash][ext]',
        pathinfo: true,
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true,
                    optimizeSSR: true,
                    sourceMap: !PROD,
                    cssSourceMap: !PROD,
                    compilerOptions: {
                        whitespace: 'condense',
                    },
                },
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                    appendTsSuffixTo: [/\.vue$/],
                },
            },
            {
                test: /\.(png|jp?eg|gif|webp)(\?.*)?$/,
                type: 'asset/resource',
                generator: {filename: 'img/[name].[hash][ext][query]'},
            },
            {
                test: /\.(woff2?|ttf|eot)(\?.*)?$/,
                type: 'asset/resource',
                generator: {filename: 'font/[name].[hash][ext]'},
            },
            {
                // SVG блокируются CORS'ом, потому для них используется
                // file-loader чтобы указать относительный publicPath.
                test: /\.(svg)(\?.*)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[hash].[ext]',
                            outputPath: 'svg',
                            publicPath: `${PUBLIC_PATH}svg/`,
                        },
                    },
                    {
                        loader: 'svgo-loader',
                        options: {
                            configFile: false,
                            plugins: [{name: 'cleanupIDs', active: false}],
                        },
                    },
                ],
            },
        ],
    },
    optimization: {
        runtimeChunk: false, // 'single'
        minimize: !!MINIFY,
        minimizer: !MINIFY ? [] : [
            new TerserPlugin({
                parallel: true,
                extractComments: false,
                terserOptions: {
                    keep_classnames: true,
                    compress: {keep_classnames: true},
                },
            }),
        ],
        splitChunks: !MINIFY ? false : {
            chunks: 'all',
            name: 'bundle',
            minChunks: 1,
            minSize: 30e3,
            maxSize: 244e3,
            automaticNameDelimiter: '.',
            cacheGroups: {
                default: false,
                commons: {
                    test: /\.js$/,
                    chunks: 'all',
                    minChunks: 2,
                    name: 'common',
                    enforce: true,
                },
                defaultVendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10,
                    reuseExistingChunk: true,
                    filename: '[name]-vendor.js',
                },
            },
        },
    },
    performance: {hints: false},
    resolve: {
        symlinks: false,
        alias: {
            '@': path.resolve(__dirname, '../src'),
            vue: path.resolve(__dirname, `../node_modules/vue`),
            'vue$': 'vue/dist/vue.esm-bundler.js',
        },
        extensions: ['.ts', '.js', '.vue', '.json'],
        modules: [path.resolve(__dirname, '../node_modules')],
    },
    plugins: [
        new VueLoaderPlugin(),
        new CleanWebpackPlugin({
            dry: false,
            verbose: false,
            cleanOnceBeforeBuildPatterns: ['**/*'],
        }),
        new webpack.DefinePlugin({
            BASE_URL: '/',
            __VUE_OPTIONS_API__: true,
            __VUE_PROD_DEVTOOLS__: false,
        }),
    ],
};
