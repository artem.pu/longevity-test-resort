import path from 'path';
import webpack, { Configuration } from 'webpack';
import 'webpack-dev-server';
import common from './client.config.js';
import { merge } from 'webpack-merge';

export default merge<Configuration>(common, {
    devtool: 'inline-source-map',
    // output: {
    //     path: path.resolve(__dirname, '../dist'),
    //     publicPath: '/',
    //     assetModuleFilename: 'asset/[name].[hash][ext]',
    //     filename: '[name].js',
    // },

    // Spin up a server for quick development
    watchOptions: {
        poll: false, // true | 1000ms,
        aggregateTimeout: 250,
    },

    devServer: {
        headers: {},
        // historyApiFallback: true,
        compress: true,
        open: false,
        port: 8080,
        hot: true,
        static: [
            {
                publicPath: '/assets',
                directory: path.resolve(__dirname, '../src/assets')
            },
            {
                publicPath: '/resort/img',
                directory: path.resolve(__dirname, '../src/assets/img')
            }
        ],
        setupMiddlewares: (middlewares, devServer) => {
            // pipeline-id
            if (!devServer.app) throw new Error('WDS.app not defined');
            devServer.app.get('/pipeline-id/', (req, res) => res.json('100500'));

            return middlewares;
        },
    },

    // plugins: [
    //     // new webpack.HotModuleReplacementPlugin(),
    // ],
})
