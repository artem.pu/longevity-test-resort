import path from 'path';
import webpack, { Configuration } from 'webpack';
import baseCfg from './base.config';
import { merge } from 'webpack-merge';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
// const ExplodeCssPlugin = require('./css/plugin');

import postcssLess from 'postcss-less';

const PROD = baseCfg.mode === 'production';
const MINIFY = process.env.MINIFY === 'minify';

const srcDir = path.resolve(__dirname, '../src');
const imgDir = path.resolve(baseCfg.output!.path!, 'img');

const cssOptions = {url: true, sourceMap: !PROD, esModule: false};

export default merge<Configuration>(baseCfg, {
    // target: PROD ? 'browserslist' : 'web',
    target: 'web',
    entry: {main: './src/entry-client.ts'},
    output: {
        // publicPath,
        // filename: PROD ? 'js/[name].[fullhash].js' : 'js/[name].[fullhash].js',
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    {loader: MiniCssExtractPlugin.loader},
                    {loader: 'css-loader', options: cssOptions},
                ]
            },
            {
                test: /\.less$/i,
                use: [
                    {loader: MiniCssExtractPlugin.loader},
                    {loader: 'css-loader', options: cssOptions},
                    {loader: 'less-loader', options: {lessOptions: {strictMath: true}}},
                    {loader: 'postcss-loader', options: {
                        postcssOptions: {syntax: postcssLess},
                    }},
                ]
            },
        ]
    },
    optimization: {
        minimizer: !MINIFY ? [`...`] : [
            `...`,
            new CssMinimizerPlugin({
                minimizerOptions: {
                    preset: ["default", {discardComments: {removeAll: true}}],
                },
            }),
        ],
    },
    plugins: [
        new webpack.ProvidePlugin({Promise: 'es6-promise'}),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[fullhash].css',
            ignoreOrder: true,
        }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: path.resolve(srcDir, 'assets/img'),
                    to: imgDir,
                },
            ],
        }),
        new HtmlWebpackPlugin({
            minfy: true,
            xhtml: true,
            title: 'resort',
            template: path.resolve(srcDir, 'index.html'),
            templateParameters: {
                props: {title: '🐼'},
            },
        }),
    ]
});
