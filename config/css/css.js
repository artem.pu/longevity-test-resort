// const fs = require('fs');
// const path = require('path');
const postcss = require('postcss');
const cssnano = require('cssnano');

async function minifyCssString(source) {
    const postCss = postcss([cssnano({preset: 'default'})]);
    let miniSource = null;
    try {
        const postResult = await postCss.process(source, {from: undefined, to: undefined});
        miniSource = postResult?.css;
    } catch(e) {
        console.log('MINIFY_CSS_ERROR', e);
        miniSource = null;
    }
    return miniSource || source;
}

async function splitCssBundle(source) {
    const limit = 220 * 1024;
    const chunksList = [];
    const miniSource = await minifyCssString(source);

    const bytes = Buffer.byteLength(miniSource, "utf-8");
    const chunkLimit = Math.floor(bytes / Math.ceil(bytes / limit));

    let css = null;
    try {css = postcss.parse(miniSource)}
    catch(e) {return console.log('SPLIT_CSS_ERROR', e), null}

    const nodes = css.nodes;
    let totalSize = 0;
    let chunkKey = 0;
    let chunkSize = 0;
    nodes.forEach((rule, k) => {
        const string = rule.toString();
        const length = Buffer.byteLength(string, "utf-8");
        chunkSize += length;

        // if (nodes.length === k + 1) console.log(`chunk: ${chunkSize/1024}`)
        // else
        if (chunkSize >= chunkLimit) {
            // console.log(`chunk: ${chunkSize/1024}`);
            chunkSize = 0;
            chunkKey += 1;
        }

        if (!chunksList[chunkKey]) chunksList[chunkKey] = [string];
        else chunksList[chunkKey].push(string);
        totalSize += length;
    });

    const result = chunksList.map(rules => rules.join(''));
    // console.log(`chunks ~ ${result.length} x ${Math.round(chunkLimit/1024)}Kb`);
    return result.length > 1 ? result : [miniSource];
}
// const cssDir = path.resolve(__dirname, '../../../build/site/client/css/');
// const src = fs.readFileSync(path.resolve(cssDir, 'bundle.45ecba60ce5c68815971.css'), {encoding: 'utf8'});
// splitCssBundle(src);
module.exports = {splitCssBundle};