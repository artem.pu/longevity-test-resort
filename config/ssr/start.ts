import {resolve as resolvePath} from 'path';
import {fork, exec, ChildProcess, ForkOptions, ExecOptions} from 'child_process';

class WebpackWatch {
    private process?:ChildProcess;
    private env = {
        NODE_ENV: 'production',
        // MINIFY: 'minify',
    };

    constructor(target:'client'|'server' = 'client') {
        const file = resolvePath(__dirname, './watch.ts');
        const forkOptions:ForkOptions = {
            stdio: 'inherit',
            env: {...process.env, ...this.env, APP_TARGET: target},
        };
        const forkArgs:string[] = [];
        const cp:ChildProcess = fork(file, forkArgs, forkOptions);
        cp.on('message', this.onMessage.bind(this));
        cp.on('error', this.onError.bind(this));

        cp.on('disconnect', () => this.onExit('disconnect'));
        cp.on('exit', () => this.onExit('exit'));
        cp.on('close', () => this.onExit('close'));
        this.process = cp;
    }

    private onExit(event:string) {
        if (this.process?.connected) console.log('die', event);
        else console.log('die', event);
    }

    private onError(event:string) {
        console.log('err', event);
    }

    private onMessage(payload:{type: string, data: any}) {
        const {type, data} = payload;
        // console.log(`${type}: ${data}`);
        if (this.callbacks[type]) this.callbacks[type].forEach(cb => cb(data));
    }

    callbacks:{[event:string]: Function[]} = {};

    public on(event:string, callback:Function) {
        if (!this.callbacks[event]) this.callbacks[event] = [callback];
        else this.callbacks[event].push(callback);
    }
}

// const bundleStatus = {server: undefined, client: undefined};
function onBundleRebuild(witch:'server'|'client') {
    if (witch === 'server') server.stdin?.write('rs\n');
    else console.log(`update at ${new Date().toISOString()}`);
}
const serverBundle = new WebpackWatch('server');
serverBundle.on('rebuild', () => onBundleRebuild('server'));
const clientBundle = new WebpackWatch('client');
clientBundle.on('rebuild', () => onBundleRebuild('client'));

const buildPath = resolvePath(__dirname, '../../../build/');
const o:ExecOptions = {cwd: buildPath};
const server:ChildProcess = exec('nodemon ./server.js --watch --inspect', o, (error, stdout, stderr) => {
    // if (stdout) console.error(`server out: ${stdout}`);
    // if (stderr) console.error(`server err: ${stderr}`);
    if (error) return console.error(`server start error: ${error}`);
});
if (server.stdout) server.stdout.pipe(process.stdout);
if (server.stderr) server.stderr.pipe(process.stderr);
// cp.stdout.on('data', (data) => console.log('\x1b[44m\x1b[37m[server]\x1b[0m', data));
// setTimeout(() => server.stdin?.write('rs\n'), 6000);