const fs = require('fs');
const path = require('path');
const dotenv = require('dotenv');
const {DefinePlugin} = require('webpack');

class ProcessEnvExtend {
    path = path.resolve(__dirname, './');
    vars = {};
    constructor() {

        const result = dotenv.config({path: `${this.path}/.env`});
        if (result.error) throw result.error;
        else this.vars = {...result.parsed};

        this.parseEnvFile(`.env.${process.env.NODE_ENV}`);
        this.parseEnvFile('.env.local', false);
    }
    parseEnvFile(filename, mandatory = true) {
        const file = path.resolve(this.path, filename);
        if (!fs.existsSync(file)) {
            if (mandatory) throw new Error(`[dotenv] file not found ${file}`);
            else return;
        }

        const json = dotenv.parse(fs.readFileSync(file) || '');
        for (const name in json) this.assignProcessEnv(name, json[name]);
    }
    assignProcessEnv(name, value) {
        if (value === process.env[name]) return;
        process.env[name] = this.vars[name] = value;
    }
    json() {
        const values = {
            ...this.vars,
            NODE_ENV: process.env.NODE_ENV,
            BASE_URL: process.env.BASE_URL,
            APP_TARGET: process.env.APP_TARGET,
        };
        return JSON.stringify(values);
    }
    webpackPlugin(varName = 'process.env') {
        return new DefinePlugin({[varName]: this.json()});
    }
}
module.exports = ProcessEnvExtend;