import * as path from 'path';
// const webpack = require('webpack');
import { Configuration } from 'webpack';
import {} from 'webpack-dev-server';
import clientCfg from './client.config';
import { merge } from 'webpack-merge';

const resortRoot = path.resolve(__dirname, '../resort');
const assetsRoot = path.resolve(__dirname, '../src/assets');

console.log('IMG', path.join(assetsRoot, 'img'));

export default merge<Configuration>(clientCfg, {
    watchOptions: {
        poll: false, // true | 1000ms,
        aggregateTimeout: 250,
    },

    devServer: {
        headers: {},
        historyApiFallback: true,
        compress: false,
        open: false,
        port: 8080,
        hot: true,
        static: [
            {
                publicPath: '/',
                directory: resortRoot,
            },
            {
                publicPath: '/resort/img',
                directory: path.join(assetsRoot, 'img'),
            },
        ],
        setupMiddlewares: (middlewares, devServer) => {
            if (!devServer) throw new Error('webpack-dev-server is not defined');
            // devServer.app?.get('*', (req, res, err) => console.log(req, res, err));
            return middlewares;
        },
    },
});

