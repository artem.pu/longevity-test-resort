import AppVue from './App.vue';
import {createSSRApp, createApp, InjectionKey} from 'vue';
import photo from '@/components/photo.vue';

// import iconComponent from '@/components/svg-icon.vue';

export function createVueApp(ssr:boolean = false) {

    const vue = (ssr ? createSSRApp : createApp)(AppVue);

    // vue.component('icon-svg', iconComponent);
    // vue.component('spinner', loadingSpinner);
    vue.component('photo', photo);

    vue.config.globalProperties.$f = {
        // rub: filterRub,
    };
    // return {router, store, app: vue};
    return {app: vue};
}