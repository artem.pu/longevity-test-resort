import sharp, { Sharp, SharpOptions, ResizeOptions, GifOptions } from 'sharp';
import glob from 'glob';
import path from 'path';

const rootDir = path.resolve(__dirname, '../');
const dataDir = path.resolve(rootDir, './src/assets/img');
const distDir = path.resolve(rootDir, './public/img');

const imageExt = /\.(png|gif|jpe?g|jfif)$/;
let images:string[] = [];
images = images.concat(glob.sync(`${dataDir}/**/*`));
images = images.filter(filePath => imageExt.test(filePath));
images = images.map(filePath => path.normalize(filePath));

const trace = 1;
const promises:Promise<any>[] = images.map(async (filePath:string) => {
    const distPath = filePath.replace(dataDir, distDir);
    const webpPath = distPath.replace(imageExt, '.webp');

    let ext:string|null|RegExpExecArray = imageExt.exec(filePath);
    ext = ext?.[1] || null;

    const options:SharpOptions = {animated: false};
    const sharpey:Sharp = sharp(filePath, options);

    // const metadata = await sharpey.metadata();
    // const width = metadata.width || 0;
    const resize:ResizeOptions = {};
    // resize.width = 400;
    // resize.width = 760;

    if (ext === 'jpg' || ext === 'jpeg') sharpey.jpeg({quality: 80});
    else if (ext === 'png') sharpey.png({quality: 80});
    else if (ext === 'gif') sharpey.gif();
    await sharpey.resize(resize).toFile(distPath);

    sharpey.webp({quality: 80})
        .resize(resize)
        .toFile(webpPath)
        .then((info) => !trace ? 1 : console.log(`${webpPath} (${Math.round(info.size/1024)}kb)`))
        .catch((err) => !trace ? 0 : console.log(`ERROR sharp: ${filePath}`, err));

    return;
});
Promise.all(promises).then(() => console.log('webp: success'));
/**/