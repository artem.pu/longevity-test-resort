/* eslint-disable no-underscore-dangle */
import {createVueApp} from './app';
const {app} = createVueApp();

if (document.readyState !== 'loading') app.mount('#app', true);
else document.addEventListener('DOMContentLoaded', () => app.mount('#app', true));
